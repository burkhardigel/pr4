package fh0010;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Fh0020Application {

	public static void main(String[] args) {
		SpringApplication.run(Fh0020Application.class, args);
	}

}
